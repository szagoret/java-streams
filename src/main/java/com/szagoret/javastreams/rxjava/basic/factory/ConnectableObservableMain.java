package com.szagoret.javastreams.rxjava.basic.factory;

import com.szagoret.javastreams.utils.AnsiColorCodes;
import io.reactivex.Observable;
import io.reactivex.observables.ConnectableObservable;

public class ConnectableObservableMain {
    public static void main(String[] args) {
        /**
         * Connectable Observable
         * all emissions are played to all Observers at once
         */

        System.out.println(
                String.format("%sFire all emissions at once %s",
                        AnsiColorCodes.ANSI_GREEN,
                        AnsiColorCodes.ANSI_RESET));

        ConnectableObservable<String> source =
                Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon")
                        .publish();

        // set up observer 1
        source.subscribe(s -> System.out.println("Observer 1: " + s));

        source.map(String::length)
                .subscribe(i -> System.out.println("Observer 2: " + i));

        // Fire!
        source.connect();
    }
}
