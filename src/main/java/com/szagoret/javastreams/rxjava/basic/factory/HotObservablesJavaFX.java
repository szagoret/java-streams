package com.szagoret.javastreams.rxjava.basic.factory;

import com.szagoret.javastreams.utils.AnsiColorCodes;
import io.reactivex.Observable;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class HotObservablesJavaFX extends Application {

    public static void main(String[] args) {
        /**
         * Hot Observables
         * A hot Observable is more like a radio station.
         * It broadcasts the same emissions to all Observers at the same time.
         * If an Observer subscribes to a hot Observable, receives some emissions,
         * and then another Observer comes in afterwards, that second Observer will
         * have missed those emissions. Just like a radio station, if you tune in too late,
         * you will have missed that song.
         */

        System.out.println(
                String.format("%sStart hot observables... %s",
                        AnsiColorCodes.ANSI_GREEN,
                        AnsiColorCodes.ANSI_RESET));
        launch(args);
        System.exit(0);

    }


    @Override
    public void start(Stage stage) throws Exception {

        //The primaryStage is the top-level container
        stage.setTitle("Hot Observables");


        //BorderLayout layout manager
        BorderPane componentLayout = new BorderPane();
        componentLayout.setPadding(new Insets(20, 0, 20, 20));

        //The FlowPane is a container that uses a flow layout
        final FlowPane choicePane = new FlowPane();
        choicePane.setHgap(100);

        ToggleButton toggleButton = new ToggleButton("TOGGLE ME");
        Label label = new Label();

        Observable<Boolean> selectedStates =
                valuesOf(toggleButton.selectedProperty());

        selectedStates.map(selected -> selected ? "DOWN" : "UP")
                .subscribe(label::setText);

        VBox vBox = new VBox(toggleButton, label);

        //Add the BorderPane to the Scene
        stage.setScene(new Scene(vBox));
        stage.show();
    }

    private static <T> Observable<T> valuesOf(final ObservableValue<T> fxObservableValue) {
        return io.reactivex.Observable.create(observableEmitter -> {

            // emit initial state
            observableEmitter.onNext(fxObservableValue.getValue());
            //emit value changes uses a listener
            final ChangeListener<T> listener =
                    (observableValue, prev, current) -> observableEmitter.onNext(current);

            fxObservableValue.addListener(listener);

        });
    }
}
