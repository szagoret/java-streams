package com.szagoret.javastreams.rxjava.basic.factory;


import com.szagoret.javastreams.utils.AnsiColorCodes;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;


public class ObservableInterface {

    public static void main(String[] args) {
        Observable<String> source =
                Observable.just("Alpha", "Beta", "Gama", "Delta", "Epsilon");


        /**
         * use inner class
         */

        System.out.println(
                String.format("%s use inner class %s",
                        AnsiColorCodes.ANSI_GREEN,
                        AnsiColorCodes.ANSI_RESET));
        Observer<Integer> myObserver = new Observer<>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Integer integer) {
                System.out.println("RECEIVED: " + integer);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
                System.out.println("Done!");
            }
        };

        source.map(String::length).filter(i -> i >= 5)
                .subscribe(myObserver);


        /**
         * use lambda
         */

        System.out.println(
                String.format("%s use lambda %s",
                        AnsiColorCodes.ANSI_GREEN,
                        AnsiColorCodes.ANSI_RESET));

        Consumer<Integer> onNext = i -> System.out.println("RECEIVED: " + i);
        Action onComplete = () -> System.out.println("Done!");
        Consumer<Throwable> onError = Throwable::printStackTrace;

        source.map(String::length).filter(i -> i >= 5)
                .subscribe(onNext, onError, onComplete);

    }
}
