package com.szagoret.javastreams.rxjava.basic.factory;

import com.szagoret.javastreams.utils.AnsiColorCodes;
import io.reactivex.Observable;

public class ColdObservables {
    public static void main(String[] args) {
        /**
         * cold Observables will replay the emissions to each Observer
         */

        System.out.println(
                String.format("%sCold observables: %s",
                        AnsiColorCodes.ANSI_GREEN,
                        AnsiColorCodes.ANSI_RESET));
        Observable<String> source =
                Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon");

        //first observer
        source.subscribe(s -> System.out.println("Observer 1 Received: " + s));

        //second observer
        source.subscribe(s -> System.out.println("Observer 2 Received: " + s));

    }
}
