package com.szagoret.javastreams.rxjava.basic.factory;

import com.szagoret.javastreams.utils.AnsiColorCodes;
import io.reactivex.Observable;

import java.util.Arrays;
import java.util.List;

public class ObservableFactory {

    public static void main(String[] args) {

        /**
         * .create
         * create an Observable from scratch by means of a function
         */

        System.out.println(
                String.format("%s use %s create %s method %s",
                        AnsiColorCodes.ANSI_GREEN,
                        AnsiColorCodes.ANSI_YELLOW,
                        AnsiColorCodes.ANSI_PURPLE,
                        AnsiColorCodes.ANSI_RESET));
        Observable<String> createSource = Observable.create(emitter -> {
            try {
                emitter.onNext("Alpha");
                emitter.onNext("Beta");
                emitter.onNext("Gamma");
                emitter.onNext("Delta");
                emitter.onNext("Epsilon");
                emitter.onComplete();
            } catch (Throwable e) {
                emitter.onError(e);
            }
        });


        // extend observable and add mapping
//        Observable<Integer> lengths = source.map(String::length);

        // extend observable and add filtering
//        Observable<Integer> filtered = lengths.filter(i -> i >= 5);

        // composed observable
        createSource.map(String::length).filter(i -> i >= 5).subscribe(s -> System.out.println("RECEIVED: " + s),
                Throwable::printStackTrace
        );

        /**
         * .just
         *  converts an item into an Observable that emits that item
         */

        System.out.println(
                String.format("%s use %s just  %s method %s",
                        AnsiColorCodes.ANSI_GREEN,
                        AnsiColorCodes.ANSI_YELLOW,
                        AnsiColorCodes.ANSI_PURPLE,
                        AnsiColorCodes.ANSI_RESET));
        Observable<String> justSource =
                Observable.just("Alpha", "Beta", "Gama", "Delta", "Epsilon");
        justSource.map(String::length).filter(i -> i >= 5).subscribe(s -> System.out.println("RECEIVED: " + s));


        /**
         *. fromIterable
         * convert various other objects and data types into Observables
         */
        System.out.println(
                String.format("%s use %s fromIterable  %s method %s",
                        AnsiColorCodes.ANSI_GREEN,
                        AnsiColorCodes.ANSI_YELLOW,
                        AnsiColorCodes.ANSI_PURPLE,
                        AnsiColorCodes.ANSI_RESET));

        List<String> items = Arrays.asList("Alpha", "Beta", "Gamma", "Delta", "Epsilon");
        Observable<String> iterableSource = Observable.fromIterable(items);
        iterableSource.subscribe(s -> System.out.println("RECEIVED: " + s));
    }
}
