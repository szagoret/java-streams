package com.szagoret.javastreams.rxjava.basic.factory;

import com.szagoret.javastreams.utils.AnsiColorCodes;
import io.reactivex.Observable;
import io.reactivex.observables.ConnectableObservable;

import java.util.concurrent.ThreadLocalRandom;

public class Multicasting {
    public static void main(String[] args) {

        /**
         * source will yield two separate emission generators,
         * and each will coldly emit a separate stream for each Observer.
         * Each stream also has its own separate map() instance,
         * hence each Observer gets different random integers
         *
         * ----------------------------------------------------
         * We call publish after map() function
         * to prevent two separate streams for each Observer
         */
        System.out.println(
                String.format("%sMulticasting - yield two separate emission generators: %s",
                        AnsiColorCodes.ANSI_GREEN,
                        AnsiColorCodes.ANSI_RESET));

        ConnectableObservable<Integer> threeRandoms = Observable.range(1, 3)
                .map(i -> randomInt()).publish();

        threeRandoms.subscribe(i -> System.out.println("Observer 1: " + i));
        threeRandoms.subscribe(i -> System.out.println("Observer 2: " + i));

        threeRandoms.connect();
    }

    private static int randomInt() {
        return ThreadLocalRandom.current().nextInt(100_000);
    }
}
